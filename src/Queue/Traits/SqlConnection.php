<?php
/**
 * SQL Connection Trait
 *
 * Enables a class to connect to the configured MySQL server. To use, include the Trait in the class definition
 * and in the constructor (or at some point before the connection is needed) call the *initSqlConnection()* method.
 *
 * @package     Flooris\Queue
 * @subpackage  Traits
 * @category    SQL
 */

namespace Flooris\Queue\Traits;

use Flooris\Config;

/**
 * SQL Connection Trait
 *
 * Enables a class to connect to the configured MySQL server. To use, include the Trait in the class definition
 * and in the constructor (or at some point before the connection is needed) call the *initSqlConnection()* method.
 */
trait SqlConnection
{
    /**
     * SQL Connection instance
     *
     * @var \PDO $sql_connection
     */
    protected $sql_connection;

    /**
     * Open connection to configured MySQL server
     *
     * @throws \Exception
     */
    protected function initSqlConnection()
    {
        try
        {
            $dsn = sprintf('mysql:dbname=%s;host=%s;port=%d', Config::get("db.connection.database"), Config::get("db.connection.host"), Config::get("db.connection.port"));
            $this->sql_connection = new \PDO($dsn, Config::get('db.connection.username'), Config::get('db.connection.password'));
        } catch (\Exception $e) {
            throw $e;
        }
    }
}