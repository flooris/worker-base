<?php
/**
 * Beanstalk Trait
 *
 * Enables a class to connect to the configured Beanstalkd server. To use, include the Trait in the class definition
 * and in the constructor (or at some point before the connection is needed) call the *initBeanstalk()* method.
 *
 * @package     Flooris\Queue
 * @subpackage  Traits
 * @category    Beanstalk
 */

namespace Flooris\Queue\Traits;

use Flooris\Config;
use Pheanstalk\Pheanstalk;

/**
 * Beanstalk Trait
 *
 * Enables a class to connect to the configured Beanstalkd server. To use, include the Trait in the class definition
 * and in the constructor (or at some point before the connection is needed) call the *initBeanstalk()* method.
 */
trait Beanstalk
{
    /**
     * Pheanstalk Connection instance
     * @var Pheanstalk
     */
    protected $connection = null;

    /**
     * Open connection to configured beanstalk server
     *
     * @throws \Exception
     */
    protected function initBeanstalk()
    {
        try
        {
            $this->connection = new Pheanstalk(Config::get('beanstalk.host'), Config::get('beanstalk.port'), Config::get('beanstalk.timeout'));
            $this->connection->useTube(Config::get('beanstalk.queue'));
        } catch (\Exception $e) {
            throw $e;
        }
    }
}