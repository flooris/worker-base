<?php

namespace Flooris\Queue\Command;

use Flooris\Config;
use Flooris\Helper\StringHelper;
use Flooris\Queue\Traits\Beanstalk;
use Pheanstalk\Exception\ServerException;
use Pheanstalk\Response\ArrayResponse;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class QueueWorkerCommand extends BaseCommand
{
    use Beanstalk;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('worker')
            ->setDescription('Queue worker / Consumer')
            ->addOption('cron', null, InputOption::VALUE_NONE, 'Run as cronjob (execute all waiting tasks, then die)');
    }

    /**
     * {@inheritdoc}
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input  = $input;
        $this->output = $output;

        $this->initBeanstalk();
        $this->canConnect();
        return $this->beginExecuteWorker();
    }

    /**
     * Check if it is possible to open a connection to the queue server
     *
     * @throws \Exception
     */
    private function canConnect()
    {
        // Check if we can connect to the server
        if ( ! $this->connection->getConnection()->isServiceListening() )
        {
            $this->throwException(sprintf('Unable to connect to beanstalk server at %s:%d', Config::get('beanstalk.host'), Config::get('beanstalk.port')));
        }

        return true;
    }

    /**
     * Start the main worker execution loop
     */
    private function beginExecuteWorker()
    {
        $is_cronjob = (bool)$this->input->getOption('cron');

        // Wait for a new task to emerge on the queue, reserve it and run the specified task command
        while( $this->canConnect() )
        {
            // Get next available job from queue
            $task = $this->connection->watch(Config::get('beanstalk.queue'))->reserve(Config::get('beanstalk.timeout'));

            // Skip rest of loop when no job is reserved
            if( ! $task )
            {
                // Output only for verbose mode, no need to spam console full of messages otherwise
                if( OutputInterface::VERBOSITY_VERBOSE <= $this->output->getVerbosity() )
                {
                    $this->output->writeln('No jobs available for now');
                }

                // Die when in cron-mode and no new queue jobs are found
                if( $is_cronjob ) {
                    return 1;
                }

                continue;
            }

            // Check if task should be buried (exceeded max retries)
            /** @var ArrayResponse $job_stats */
            $job_stats = $this->connection->statsJob($task);
            $reserves  = $job_stats['reserves'];

            // Check if task exceeds max reserve count
            if( $reserves >= Config::get('beanstalk.max_reserves') ) {
                $this->writeOutput('Task %d has exceeded max task retries.', array($task->getId()), LogLevel::ERROR);
                $this->buryTask($task);
            }

            // Retrieve data from task
            $data = $task->getData();
            $data = json_decode($data, true);

            // Validate if this task contains a valid item name
            if( !!! $data OR ! array_key_exists('item_name', $data )) {
                $this->buryTask($task);

                $this->writeOutput('Task %d Failed (json error: %s). %s', array($task->getId(), json_last_error(), $task->getData()), LogLevel::ERROR);
                continue;
            }

            // Check if required arguments parameter is included in task
            if( ! array_key_exists('arguments', $data) || ! is_array($data['arguments']))
            {
                $this->buryTask($task);
                $this->writeOutput('Task %d does not have the required "arguments" item. %s', array($task->getId(), $task->getData()), LogLevel::ERROR);

                continue;
            }

            // Format task data as a Command name
            $command_name = $this->getFormattedCommandName($data['item_name']);

            // Check if the command is really a task defined by the application
            if( $this->isValidCommand($command_name) )
            {
                $arguments = (is_array($data['arguments'])) ? $data['arguments'] : array();
                $result = $this->executeCommand( $command_name, $arguments );

                // Exit code of 0 means successful execution
                if( 0 == $result )
                {
                    // Task completed successfully, so we mark it as completed on the queue server
                    $this->connection->delete($task);
                } else {
                    $this->writeOutput('Task %d exited with a unexpected exit_code of: %d %s', array($task->getId(), $result, $task->getData()));
                    $this->buryTask($task);
                }
            }
            else
            {
                $this->writeOutput('Task %d Failed, command %s is not registered in Application. %s', array($task->getId(), $command_name, $task->getData()), LogLevel::ERROR);

                // Bury command for now
                $this->buryTask($task);

                continue;
            }

        }
    }

    /**
     * Check if the given Command name is a valid Command registered with the current Application
     *
     * @param $command_name string Command name to validate
     * @return bool
     */
    private function isValidCommand($command_name)
    {
        return $this->getApplication()->has($command_name);
    }

    /**
     * Execute given Command from the current Application
     *
     * @param $command_name string Name of Command to execute
     * @param array $arguments Arguments to pass to the command
     * @return int Exit code
     * @throws \Exception
     */
    private function executeCommand($command_name, $arguments = array())
    {
        $command = $this->getApplication()->find($command_name);
        $arguments['command'] = $command_name;

        $input = new ArrayInput($arguments);

        try
        {
            return $command->run($input, $this->output);
        }
        catch(\Exception $e) {
            $this->writeOutput('Command "%s" failed to execute, %s, %s', array($command_name, $e->getMessage(), print_r($arguments, true)), LogLevel::ERROR);

            // Command did not execute properly, return error code
            return -1;
        }
    }

    /**
     * Format command name to the normalized format
     *
     * @param $name string
     * @return string
     */
    private function getFormattedCommandName($name)
    {
        // Convert name from snake_case to CamelCase
        $name_cc = StringHelper::snakeToCamel($name);
        $command_name = $name_cc . 'Command';

        return $command_name;
    }

    /**
     * Attempt to bury the given task, method is used to ensure worker doesn't crash when a task can't be burried correctly
     *
     * @param $task
     */
    private function buryTask($task)
    {
        try
        {
            $this->connection->bury($task);
        }
        catch( ServerException $e )
        {
            $this->writeOutput('Burying task failed with: %s', array($e->getMessage()), LogLevel::ERROR);
        }
    }
}
