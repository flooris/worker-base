<?php
/**
 *
 */

namespace Flooris\Queue\Command;

use Flooris\Config;
use Psr\Log\LogLevel;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class BaseCommand
 *
 * @package Flooris\Queue\Command
 */
abstract class BaseCommand extends Command
{
    const RESULT_SUCCESS = 1;

    /** @var InputInterface $input */
    protected $input;

    /** @var OutputInterface $output */
    protected $output;

    /**
     * Throws Exception specified by $message
     * Notifies Sentry of exception when enabled in configuration
     *
     * @param $message string Error message
     * @throws \Exception
     */
    protected function throwException($message)
    {
        $exception = new \Exception($message);

        throw $exception;
    }

    /**
     * Write message to Command output interface
     *
     * @param $message string Message to write to output, Follows printf syntax
     * @param array $arguments Array of parameters used for substitution in $message
     * @param string $level Message verbosity level
     */
    protected function writeOutput($message, $arguments = array(), $level = LogLevel::INFO)
    {
        // Rewrite message to include any formatting parameters
        $message = vsprintf($message, $arguments);

        // Add formatting based on debug level to message (only used for console output)
        $console_message = sprintf('<%1$s>%2$s</%1$s>', $level, $message);

        $this->output->writeln($console_message);
    }

    /**
     * {@inheritdoc}
     */
    public function run(InputInterface $inputInterface, OutputInterface $outputInterface)
    {
        $this->input = $inputInterface;
        $this->output = $outputInterface;

        return parent::run($inputInterface, $outputInterface);
    }
}