<?php

namespace Flooris;

use Illuminate\Config\FileLoader;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Config\Repository;

class Config
{
    private static $_instance = null;

    /**
     * @return Repository
     */
    public static function getInstance()
    {
        if ( null === static::$_instance )
        {
            static::$_instance = static::createInstance();
        }

        return static::$_instance;
    }

    /**
     * @return Repository
     */
    private static function createInstance()
    {
        $load   = new FileLoader(new Filesystem(), __DIR__ . '/../config');
        $config = new Repository($load, 'production');

        return $config;
    }

    /**
     * Get Config value from specific config file
     * Example: Config::get('filename.key')
     *
     * @param $key
     * @param null $default
     *
     * @return mixed
     */
    public static function get( $key, $default = null )
    {
        return static::getInstance()->get($key, $default);
    }
}