<?php

namespace Flooris\Helper;

class StringHelper
{
    /**
     * Convert string in snake_case to a string in CamelCase
     * @param $string string
     * @return String
     */
    public static function snakeToCamel($string)
    {
        return str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
    }
}